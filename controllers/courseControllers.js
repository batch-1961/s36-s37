const Course = require("../models/Course");

module.exports.getAllCourses = (req,res) =>{

	Course.find({})
	.then(result => res.send(result))
	.catch(error => res.send(error))

};


module.exports.addCourse = (req,res)=>{

	let newCourse = new Course({

		name: req.body.name,
		description: req.body.description,
		price: req.body.price 

	})

	// console.log(newCourse);

	newCourse.save()
	.then(result => res.send(result))
	.catch(error => res.send(error))

};

module.exports.getActiveCourses = (req,res) => {

	Course.find({isActive: true})
	.then(result => res.send(result))
	.catch(error => res.send(error))
	
}

//activity

module.exports.getCourseById = (req,res) => {

	console.log(req.params.courseId)

// Course.findOne({_id:req.body.id})
	Course.findById(req.params.courseId)
	.then(result => res.send(result))
	.catch(error => res.send(error))
	
}


module.exports.updateCourse = (req,res) => {

	console.log(req.params.courseId);
	console.log(req.body);
	let update = {
		"name": "ReactJS 101",
    	"description": "Learn ReactJS",
    	"price": 25000

	}

	Course.findByIdAndUpdate(req.params.courseId,update,{new:true})
	.then(result => res.send(result))
	.catch(error => res.send(error))
	
}


module.exports.archiveCourse = (req,res) => {
	console.log(req.params.courseId);

	let courseToArchive = {isActive:false};
	Course.findByIdAndUpdate(req.params.courseId,courseToArchive,{new:true})
	.then(result => res.send(result))
	.catch(error => res.send(error))
	
}