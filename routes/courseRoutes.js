

const express = require("express");
const router = express.Router();

const courseControllers = require("../controllers/courseControllers");

const auth = require("../auth");
const {verify,verifyAdmin} = auth;


router.get('/',verify,verifyAdmin,courseControllers.getAllCourses);

router.post('/',verify,verifyAdmin,courseControllers.addCourse);

//for non registered user
router.get('/activeCourses',courseControllers.getActiveCourses);

router.get('/getSingleCourse/:courseId',courseControllers.getCourseById);

router.put('/updateCourse/:courseId',verify,verifyAdmin,courseControllers.updateCourse);
router.delete("/archiveCourse/:courseId",verify,verifyAdmin,courseControllers.archiveCourse);

module.exports = router;
