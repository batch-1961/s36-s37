

const express = require("express");
const router = express.Router();

const userControllers = require("../controllers/userControllers");

// console.log(userControllers);

const auth = require("../auth");
const {verify} = auth;



router.post('/',userControllers.registerUser);

router.get('/details',verify,userControllers.getUserDetails);

//USER Authentication

router.post('/login',userControllers.loginUser);

//checkemail activity

router.post('/checkEmail',userControllers.checkEmail);

router.post('/enroll',verify,userControllers.enroll);

module.exports = router;
